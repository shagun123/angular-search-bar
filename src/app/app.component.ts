import {Component, OnDestroy, OnInit} from '@angular/core';
import {BehaviorSubject, combineLatest, Subject} from 'rxjs';
import {takeWhile} from 'rxjs/operators';

@Component({
  selector: 'youtube-root',
  template: `
    <div fxLayout="column" fxLayoutAlign="start center">
      <youtube-search-bar (search)="this.search$.next($event)"></youtube-search-bar>
      <youtube-video-list [videoList]="this.finalVideos"></youtube-video-list>
    </div>
  `,
  styles: [``]
})

export class AppComponent implements OnInit, OnDestroy {
  search$ = new Subject<string>();
  data = [{
    title: 'testing video',
    description: 'i am a description of vide'
  }, {
    title: 'video ab 2',
    description: 'i am a description of vide'
  }, {
    title: 'video cd 3',
    description: 'i am a description of vide'
  }, {
    title: 'test video 4',
    description: 'i am a description of vide'
  }, {
    title: 'test abc video 5',
    description: 'i am a description of vide'
  }];
  finalVideos = [];
  videos$ = new BehaviorSubject<any[]>(this.data);
  isAlive = true;

  constructor() {
  }

  ngOnInit(): void {
    combineLatest([this.search$, this.videos$])
      .pipe(takeWhile(() => this.isAlive = true)).subscribe(data => {
      const search = data[0];
      const videos = data[1];
      if (!search) {
        this.finalVideos = videos;
      } else {
        this.finalVideos = videos.filter((video) => video.title.toLowerCase()
          .indexOf(search.toLowerCase()) !== -1);
      }
    });
  }

  ngOnDestroy() {
    this.isAlive = false;
  }
}
