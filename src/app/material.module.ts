import {NgModule} from '@angular/core';
import {MatFormFieldModule} from '@angular/material/form-field';
import {MatIconModule} from '@angular/material/icon';
import {MatCardModule} from '@angular/material/card';


const modules = [
  MatFormFieldModule,
  MatIconModule,
  MatCardModule
];

@NgModule({
  imports: modules,
  exports: modules
})

export class MaterialModule {
}
