import {Component, Input} from '@angular/core';

@Component({
  selector: 'youtube-video-card',
  template: `
    <mat-card>
      <div>
        <span>{{this.video.title}}</span>
      </div>
      <div>
        <span>{{this.video.description}}</span>
      </div>
    </mat-card>
  `,
  styles: [``]
})

export class VideoCardComponent {
  @Input() video: any;
}
