import {Component, Input} from '@angular/core';

@Component({
  selector: 'youtube-video-list',
  template: `
    <div fxlayoutwrap fxLayoutAlign="center stretch" fxLayoutGap="30px">
      <youtube-video-card *ngFor="let video of this.videoList"
                          [video]="video"></youtube-video-card>
    </div>
  `,
  styles: [``]
})

export class VideoListComponent {
  @Input() videoList: any[];
}
