import {Component, EventEmitter, OnInit, Output} from '@angular/core';
import {FormControl} from '@angular/forms';
import {debounceTime, distinctUntilChanged, startWith} from 'rxjs/operators';

@Component({
  selector: 'youtube-search-bar',
  template: `
    <mat-form-field fxLayoutAlign="start center" style="margin-top: 2%">
      <mat-placeholder>
        <mat-icon>search</mat-icon>
        <span>Search</span>
      </mat-placeholder>
      <input matInput [formControl]="this.searchField"/>
    </mat-form-field>
  `,
  styles: [``]
})

export class SearchBarComponent implements OnInit {
  searchField: FormControl;
  @Output() search = new EventEmitter<string>();

  constructor() {
  }

  ngOnInit(): void {
    this.searchField = new FormControl();
    this.searchField.valueChanges.pipe(debounceTime(250),
      distinctUntilChanged(), startWith('')).subscribe(data => {
      this.search.emit(data);
    });
  }
}
